package main

import (
	"testing"
)

// test function
func TestReturnCyberGirls(t *testing.T) {
	actualString := ReturnCyberGirls()
	expectedString := "Cybergirls2.0"
	if actualString != expectedString {
		t.Errorf("Expected String(%s) is not same as"+
			" actual string (%s)", expectedString, actualString)
	}
}
