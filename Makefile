
build:
	@echo "Building and packaging application"
	go build -o bin/

lint:
	@echo "Linting application"
	golangci-lint run

test:
	@echo "Testing our application"
	go test -v --cover