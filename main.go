package main

import (
	"fmt"
)

func ReturnCyberGirls() string {
	return "Cybergirls2.0"
}

func main() {
	fmt.Println("This is a provenance lab test package.")
	ReturnCyberGirls()
}
